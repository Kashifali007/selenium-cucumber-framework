package stepDefinations;

import com.qa.BaseTest;
import com.qa.Pages.AmazonLoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AmazonLoginStepDefination {
    AmazonLoginPage amazonLoginPage;
    BaseTest baseTest;

    public AmazonLoginStepDefination(){
        amazonLoginPage = new AmazonLoginPage();
        baseTest = new BaseTest();
    }

    @Given("User is at Amazon home screen")
    public void user_is_at_amazon_home_screen() {
        baseTest.softAssert.assertEquals(baseTest.getPageTitle(),"Amazon.in: Beauty deals: Beauty");
    }
    @When("User click on Sign in button")
    public void user_click_on_sign_in_button() {
       amazonLoginPage.clickonHomeScreenSign();
    }
    @When("User enter the email {string}")
    public void user_enter_the_email(String email) {
        amazonLoginPage.enterEmail(email);
    }
    @When("User click on Continue button")
    public void user_click_on_continue_button() {
        amazonLoginPage.clickOnContinueButton();
    }
    @When("User enter the password {string}")
    public void user_enter_the_password(String password) {
        amazonLoginPage.enterPassword(password);
    }
    @Then("User should be able to login")
    public void user_should_be_able_to_login() {
        amazonLoginPage.clickOnSignbutton();
    }

}

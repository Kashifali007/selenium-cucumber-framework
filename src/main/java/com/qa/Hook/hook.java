package com.qa.Hook;

import com.qa.BaseTest;
import io.cucumber.java.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

public class hook {

    Logger log;
    BaseTest baseTest;
    public hook(){

        log = (Logger) LogManager.getLogger(hook.class);
        baseTest = new BaseTest();
    }

    @BeforeStep
    public void beforeStep(Scenario scenario){
        log.info("executing hook class BeforeStep method");

    }

    @AfterStep
    public void afterStep(){
        log.info("executing hook class AfterStep method");
    }

    @Before
    public void before(){
        log.info("executing hook class Before method");
    }

    @After
    public void after(){
        log.info("executing hook class After method");

    }
}

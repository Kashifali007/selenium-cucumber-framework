package com.qa;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

@CucumberOptions(
        features = "src/test/resources/Features/",
        glue = {"stepDefinations", "com.qa.Hook"},
        tags = "@Smoke",
        plugin = {"pretty",
        "html:target/cucumber-reports/cucumber-pretty",
        "json:target/cucumber-reports/CucumberTestReport.json"})


public class TestRunner extends AbstractTestNGCucumberTests {

    BaseTest baseTest;
    Logger log;

    public TestRunner(){
        log =(Logger)LogManager.getLogger(TestRunner.class);
        baseTest = new BaseTest();
    }


    @BeforeClass(alwaysRun = true)
    @Parameters({"browser", "configFileName", "OS"})
    public void setUpClass(String browser, String configFileName, String os) {
        log.info("executing setUpClass method [BeforeClass annotation]");
        log.info("browser [" +browser + "]");
        log.info("browser [" +browser + "]");
        log.info("browser [" +browser + "]");
        baseTest.setupDriver(browser,configFileName,os);
        }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() {
        log.info("executing tearDownClass method [AfterClass annotation]");
        baseTest.driverTearDown();
    }
    }



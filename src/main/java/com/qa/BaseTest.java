package com.qa;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentReporter;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.qa.Utils.TestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;


public class BaseTest {

    Properties props = new Properties();
    private static WebDriver driver = null;
    public  ExtentReports extentReports;
    public ExtentTest extentTest;
    TestUtils utils;
    public SoftAssert softAssert;
    Logger log;

    public BaseTest() {
        log = (Logger) LogManager.getLogger(BaseTest.class);
        utils = new TestUtils();
        softAssert = new SoftAssert();
    }

    // webdriver getter method
    public WebDriver getDriver(){
        return driver;
    }

    // loading properties file here
    public void loadAllProperties(String configFileName) {
        try {

            log.info("Executing loadAllProperties method");
            String fileName = System.getProperty("user.dir") + File.separator + "src" +File.separator + "main" + File.separator + "resources"+ File.separator +configFileName;
            log.info("File name == " + fileName);
            props.load(new FileInputStream(fileName));
        }catch (IOException e){
            log.error("File not found at current location [" + configFileName + "]");
        }
    }

    // initializing webdriver here
    public void setupDriver(String browser, String configFileName, String os){
        log.info("Setting up WebDriver with browser[" + browser +"]" + "configFileName[" + configFileName + "]" + "OS [" + os +"]");
        loadAllProperties(configFileName);

        if(browser.equalsIgnoreCase("chrome")&& os.equalsIgnoreCase("Windows")){
            log.info("setting up chrome driver on windows");
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver.exe" );
            driver = new ChromeDriver();
        }

        else if(browser.equalsIgnoreCase("chrome")&& os.equalsIgnoreCase("Linux")){
            log.info("setting up chrome driver on linux");
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver" );
            driver = new ChromeDriver();
        }

        else if(browser.equalsIgnoreCase("headless")&& os.equalsIgnoreCase("windows")){
            log.info("setting up headless chrome driver on window");
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver.exe" );
            driver = new ChromeDriver();
        }

        else if(browser.equalsIgnoreCase("headless")&& os.equalsIgnoreCase("linux")){
            log.info("setting up headless chrome driver on linux");
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver" );
            ChromeOptions options = new ChromeOptions();
            options.addArguments("window-size=1400,800");
            options.addArguments("headless");
            options.addArguments("disable-gpu");
            options.addArguments("no-sandbox");
            options.addArguments("allow-insecure-localhost");
            options.addArguments("disable-dev-shm-usage");

            driver = new ChromeDriver(options);

        }

        else if(browser.equalsIgnoreCase("firefox") && os.equalsIgnoreCase("windows")){
            log.info("setting up firefox driver on windows");
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "geckodriver.exe" );
            driver = new FirefoxDriver();
        }

        else if(browser.equalsIgnoreCase("firefox") && os.equalsIgnoreCase("linux")){
            log.info("setting up firefox driver on linux");
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "geckodriver" );
            driver = new FirefoxDriver();
        }

        else {
            log.error("pass browser parameter [chrome/firefox] and pass OS parameter [window/linux] in testng.xml");
             throw new RuntimeException(new WebDriverException("driver not initialized"));
        }

        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

    }

    public void driverTearDown(){
            log.info("teardown : exiting webdriver");
            driver.quit();
    }

    // setting up html extent report here
    public ExtentReports setupExtentReporter(){
        log.info("setting up extent report");
        extentReports = new ExtentReports();
        ExtentSparkReporter sparkReporter = new ExtentSparkReporter(System.getProperty("user.dir") +
                File.separator + "html-report/ExecutionReport_"+utils.getDateTime()+".html" );
        extentReports.attachReporter(sparkReporter);

        sparkReporter.config().setDocumentTitle("Document Title");
        sparkReporter.config().setTheme(Theme.DARK);
        sparkReporter.config().setReportName("Regression Test Results");
        return extentReports;
    }

    // webdriver wrapper methods

    public WebElement waitforvisibilityofElement(By elementLocation){
        log.info("executing waitforvisibilityofElement");
        return new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.presenceOfElementLocated(elementLocation));

    }

    public WebElement waitForElementToBeClickable (By elementLocation){
        log.info("executing waitForElementToBeClickable method");
        return new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(elementLocation));
    }

    public void ClickOnElement( By locator){
        log.info("executing ClickOnElement method");
        driver.findElement(locator).click();
    }

    public String getElementText(By locator){
        log.info("executing getElementText method");
        return driver.findElement(locator).getText();
    }

    public void sendTextToElement(By locator, String text){
        log.info("executing sendTextToElement method");
        driver.findElement(locator).sendKeys(text);
    }

    public void moveToElement(By locator){
        log.info("executing moveToElement method");
        new Actions(driver).moveToElement(driver.findElement(locator)).build().perform();
    }

    public String getPageTitle(){
        log.info("executing getPageTitle method");
        return driver.getTitle();
    }

} // End of class


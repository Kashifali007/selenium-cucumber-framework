package com.qa.Pages;

import com.qa.BaseTest;
import lombok.extern.java.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.openqa.selenium.By;

public class AmazonLoginPage {

    BaseTest baseTest;
    Logger log;
    public AmazonLoginPage(){
        log = (Logger) LogManager.getLogger(AmazonLoginPage.class);
        log.info("executing AmazonLoginPage constructor");
        baseTest = new BaseTest();
    }

    By SignButton = By.cssSelector("#nav-link-accountList-nav-line-1");
    By email = By.cssSelector("#ap_email");
    By continueButton = By.xpath("//input[@id='continue']");
    By password = By.xpath("//input[@id='ap_password']");
    By sbutton = By.xpath("//input[@id='signInSubmit']");



    public void clickonHomeScreenSign(){
        log.info("executing click on HOme screen");
        baseTest.waitforvisibilityofElement(SignButton);
        baseTest.ClickOnElement(SignButton);
    }

    public void enterEmail(String emailText){
        baseTest.waitforvisibilityofElement(email);
        baseTest.sendTextToElement(email, emailText);
    }

    public void clickOnContinueButton(){
        baseTest.waitforvisibilityofElement(continueButton);
        baseTest.ClickOnElement(continueButton);
    }

    public void enterPassword(String passwordText){
        baseTest.waitforvisibilityofElement(password);
        baseTest.sendTextToElement(password, passwordText);
    }

    public void clickOnSignbutton(){
        baseTest.waitforvisibilityofElement(sbutton);
        baseTest.ClickOnElement(sbutton);
    }

}
